// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA7DRXdk50XfHlNniLezO8xJcxwwGy6AVE",
    authDomain: "app-ifinancas.firebaseapp.com",
    projectId: "app-ifinancas",
    storageBucket: "app-ifinancas.appspot.com",
    messagingSenderId: "992655029333",
    appId: "1:992655029333:web:7bc6360541faa51a48d0dc",
    measurementId: "G-VNZYEXKHZP"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
