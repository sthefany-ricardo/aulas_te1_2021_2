## iFinanças - App de Controle Financeiro

O **iFinanças** é um app para controle financeiro pessoal.

O app foi desenvolvido durante as aulas de Tópicos Especiais I, disciplina do curso de Análise e Desenvolvimento de
Sistemas do IFSP Campus Guarulhos.

---
## Ferramenta e linguagens

As linguaguens utilizadas para o desenvolvimento do app são: 

- **Node.js** 
- **Ionic Framework** 
- **Ionic Angular**
- **Firebase**

As ferramentas utilizadas no desenvolvimento do app são: 

- **Visual Studio Code** 
- **Git** (para controle de versão)
- **Bitbucket**
- **ScreenToGif** (para fazer os gifs de demonstração)

---
### Logo do app iFinanças

**Logo iFinanças**  

![Logo iFinanças](/src/assets/icon/Logo%20iFinan%C3%A7as.png)

**Logo iFinanças com fundo transparente**  

![Logo iFinanças fundo transparente](/src/assets/icon/Logo%20iFinan%C3%A7as%20transparente.png) 

---